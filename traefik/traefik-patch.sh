kubectl --context=k3d-toy1 --namespace=kube-system patch deploy traefik --patch "$(cat traefik-patch.json)"
kubectl --context=k3d-toy1 --namespace=kube-system patch deploy traefik --type='json' --patch "$(cat traefik-port-patch.json)"
