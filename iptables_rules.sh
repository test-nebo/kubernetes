#!/bin/bash

# ============= function ==============

function enable_routing {

    sudo iptables -t nat -A PREROUTING -s 127.0.0.1 -p tcp --dport 80 -j REDIRECT --to 80
    sudo iptables -t nat -A OUTPUT -s 127.0.0.1 -p tcp --dport 80 -j REDIRECT --to 80
    sudo iptables -t nat -A PREROUTING -s 127.0.0.1 -p tcp --dport 443 -j REDIRECT --to 443
    sudo iptables -t nat -A OUTPUT -s 127.0.0.1 -p tcp --dport 443 -j REDIRECT --to 443
    echo "routing enabled"
}

function disable_routing {

    sudo iptables -t nat -D PREROUTING -s 127.0.0.1 -p tcp --dport 80 -j REDIRECT --to 80
    sudo iptables -t nat -D OUTPUT -s 127.0.0.1 -p tcp --dport 80 -j REDIRECT --to 80
    sudo iptables -t nat -D PREROUTING -s 127.0.0.1 -p tcp --dport 443 -j REDIRECT --to 443
    sudo iptables -t nat -D OUTPUT -s 127.0.0.1 -p tcp --dport 443 -j REDIRECT --to 443
    echo "routing disabled"
}

function run {

    if [ "enable" = "$1" ]; then
      enable_routing $1

    elif [ "disable" = "$1" ]; then
      disable_routing $1

    else
        echo "не корретный аргумент при вызове ${FUNCNAME[0]}"
    fi

}

# ============= params ==============

RED="\033[0;31m"
NORMAL="\033[0m"


# ============= execute ==============

if [[ -z $1 ]]; then

    printf "\nнадо добавить аргумент:
    ${RED}enable${NORMAL}     добавить роутинг
    ${RED}disable${NORMAL}    удалить роутинг\n"

else
    run $1
    sudo iptables-save | egrep '8000|8443' | grep -v DOCKER
fi
