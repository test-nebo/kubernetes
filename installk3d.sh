k3d cluster create --config k3d-cluster-toy.yml
# --k3s-arg "--no-deploy=traefik@server:*"
# kubectl create deployment nginx --image=nginx
# kubectl create service clusterip nginx --tcp=80:80 --tcp=443:443

#
#     spec:
#      volumes:
#        - name: data
#          persistentVolumeClaim:
#            claimName: pv-traefik-claim
#        - name: tmp
#          emptyDir: {}
#      containers:
#        - name: traefik
#          image: rancher/mirrored-library-traefik:2.6.2
#          args:
#            - '--global.checknewversion'
#            - '--global.sendanonymoususage'
#            - '--entrypoints.metrics.address=:9100/tcp'
#            - '--entrypoints.traefik.address=:9000/tcp'
#            - '--entrypoints.web.address=:80/tcp'
#            - '--entrypoints.websecure.address=:443/tcp'
#            - '--api.dashboard=true'
#            - '--api.insecure=true'
#            - '--ping=true'
#            - '--metrics.prometheus=true'
#            - '--metrics.prometheus.entrypoint=metrics'
#            - '--providers.kubernetescrd'
#            - '--providers.kubernetesingress'
#            - '--providers.kubernetesingress.allowexternalnameservices=true'
#            - '--providers.kubernetescrd.allowexternalnameservices=true'
#            - >-
#              --providers.kubernetesingress.ingressendpoint.publishedservice=kube-system/traefik
#            - >-
#              --certificatesresolvers.letsencrypt.acme.email=pavelkuzin@gmail.com
#            - '--certificatesresolvers.letsencrypt.acme.storage=/data/acme.json'
#            - '--certificatesresolvers.letsencrypt.acme.httpchallenge=true'
#            - >-
#              --certificatesresolvers.letsencrypt.acme.httpchallenge.entrypoint=web
#          ports:
#            - name: metrics
#              containerPort: 9100
#              protocol: TCP
#            - name: traefik
#              containerPort: 9000
#              protocol: TCP
#            - name: web
#              containerPort: 80
#              protocol: TCP
#            - name: websecure
#              containerPort: 443
#              protocol: TCP
#          resources: {}
#          volumeMounts:
#            - name: data
#              mountPath: /data
#            - name: tmp
#              mountPath: /tmp
