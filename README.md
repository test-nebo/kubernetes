# Demo 

https://nebo.pavelkuzin.ru

# k3d - example

Пример простого запуска kubernetes с traefik + letsencrypt

- быстро разворачивается на любой виртуалке
- минимальное требование к ресурсам. т.к. это обертка для k3s
- работает в docker, не требует иных зависимостей. идеален для разработки

![screenshot](https://i.imgur.com/LNaZMZq.png "Пример работы k3d с 11 поднятыми проектами (back, front, cronjob)")
